EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dk_Embedded-Microcontrollers:ATSAMD21G18A-AUT U1
U 1 1 61A223B2
P 7250 3500
F 0 "U1" H 6900 5050 60  0000 C CNN
F 1 "ATSAMD21G18A-AUT" H 8150 2250 60  0000 C CNN
F 2 "digikey-footprints:TQFP-48_7x7mm" H 7450 3700 60  0001 L CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en605782" H 7450 3800 60  0001 L CNN
F 4 "ATSAMD21G18A-AUTCT-ND" H 7450 3900 60  0001 L CNN "Digi-Key_PN"
F 5 "ATSAMD21G18A-AUT" H 7450 4000 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 7450 4100 60  0001 L CNN "Category"
F 7 "Embedded - Microcontrollers" H 7450 4200 60  0001 L CNN "Family"
F 8 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en605782" H 7450 4300 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/microchip-technology/ATSAMD21G18A-AUT/ATSAMD21G18A-AUTCT-ND/4878879" H 7450 4400 60  0001 L CNN "DK_Detail_Page"
F 10 "IC MCU 32BIT 256KB FLASH 48TQFP" H 7450 4500 60  0001 L CNN "Description"
F 11 "Microchip Technology" H 7450 4600 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7450 4700 60  0001 L CNN "Status"
	1    7250 3500
	1    0    0    -1  
$EndComp
$Comp
L dk_USB-DVI-HDMI-Connectors:10118194-0001LF J1
U 1 1 61A253B5
P 1950 2200
F 0 "J1" H 2014 2945 60  0000 C CNN
F 1 "10118194-0001LF" H 2014 2839 60  0000 C CNN
F 2 "digikey-footprints:USB_Micro_B_Female_10118194-0001LF" H 2150 2400 60  0001 L CNN
F 3 "http://www.amphenol-icc.com/media/wysiwyg/files/drawing/10118194.pdf" H 2150 2500 60  0001 L CNN
F 4 "609-4618-1-ND" H 2150 2600 60  0001 L CNN "Digi-Key_PN"
F 5 "10118194-0001LF" H 2150 2700 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 2150 2800 60  0001 L CNN "Category"
F 7 "USB, DVI, HDMI Connectors" H 2150 2900 60  0001 L CNN "Family"
F 8 "http://www.amphenol-icc.com/media/wysiwyg/files/drawing/10118194.pdf" H 2150 3000 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/amphenol-icc-fci/10118194-0001LF/609-4618-1-ND/2785382" H 2150 3100 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN RCPT USB2.0 MICRO B SMD R/A" H 2150 3200 60  0001 L CNN "Description"
F 11 "Amphenol ICC (FCI)" H 2150 3300 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2150 3400 60  0001 L CNN "Status"
	1    1950 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 61A2821A
P 1850 2850
F 0 "#PWR01" H 1850 2600 50  0001 C CNN
F 1 "GND" H 1855 2677 50  0000 C CNN
F 2 "" H 1850 2850 50  0001 C CNN
F 3 "" H 1850 2850 50  0001 C CNN
	1    1850 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 61A2882A
P 2250 2500
F 0 "#PWR04" H 2250 2250 50  0001 C CNN
F 1 "GND" H 2255 2327 50  0000 C CNN
F 2 "" H 2250 2500 50  0001 C CNN
F 3 "" H 2250 2500 50  0001 C CNN
	1    2250 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 2400 2250 2500
Wire Wire Line
	1850 2800 1850 2850
$Comp
L power:+5V #PWR05
U 1 1 61A2A32F
P 3150 1950
F 0 "#PWR05" H 3150 1800 50  0001 C CNN
F 1 "+5V" H 3165 2123 50  0000 C CNN
F 2 "" H 3150 1950 50  0001 C CNN
F 3 "" H 3150 1950 50  0001 C CNN
	1    3150 1950
	1    0    0    -1  
$EndComp
Text Label 2350 2100 0    50   ~ 0
usbD-
Text Label 2350 2200 0    50   ~ 0
usbD+
Wire Wire Line
	2250 2100 2350 2100
Wire Wire Line
	2250 2200 2350 2200
Wire Wire Line
	3150 2000 3000 2000
Wire Wire Line
	3150 2000 3150 1950
$Comp
L power:GND #PWR014
U 1 1 61A3C757
P 7400 5000
F 0 "#PWR014" H 7400 4750 50  0001 C CNN
F 1 "GND" H 7405 4827 50  0000 C CNN
F 2 "" H 7400 5000 50  0001 C CNN
F 3 "" H 7400 5000 50  0001 C CNN
	1    7400 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 4900 7250 4950
Wire Wire Line
	7250 4950 7350 4950
Wire Wire Line
	7550 4950 7550 4900
Wire Wire Line
	7450 4900 7450 4950
Connection ~ 7450 4950
Wire Wire Line
	7450 4950 7550 4950
Wire Wire Line
	7350 4900 7350 4950
Connection ~ 7350 4950
Wire Wire Line
	7350 4950 7400 4950
Wire Wire Line
	7400 4950 7400 5000
Connection ~ 7400 4950
Wire Wire Line
	7400 4950 7450 4950
$Comp
L 805_bobbin:Bob L1
U 1 1 61A44ADD
P 6500 1550
F 0 "L1" V 6454 1605 50  0000 L CNN
F 1 "MH2029-300Y" V 6545 1605 50  0000 L CNN
F 2 "Inductor_SMD:L_0805_2012Metric" H 6500 1550 50  0001 L BNN
F 3 "" H 6500 1550 50  0001 L BNN
	1    6500 1550
	0    1    1    0   
$EndComp
Wire Wire Line
	7550 1900 7550 1800
Wire Wire Line
	7550 1800 8000 1800
Connection ~ 8150 1250
Wire Wire Line
	8150 1250 8300 1250
Connection ~ 7700 1250
Wire Wire Line
	7450 1900 7450 1850
Wire Wire Line
	7450 1250 7700 1250
Wire Wire Line
	7250 1900 7250 1800
Wire Wire Line
	7350 1900 7350 1850
Wire Wire Line
	7350 1850 7450 1850
Connection ~ 7450 1850
Wire Wire Line
	7450 1850 7450 1800
Wire Wire Line
	7250 1800 7450 1800
Connection ~ 7450 1800
Wire Wire Line
	7450 1250 7450 1800
Wire Wire Line
	6500 1800 7150 1800
Wire Wire Line
	7150 1800 7150 1900
Wire Wire Line
	6500 1750 6500 1800
$Comp
L power:GND #PWR012
U 1 1 61A5C5B0
P 6500 2150
F 0 "#PWR012" H 6500 1900 50  0001 C CNN
F 1 "GND" H 6505 1977 50  0000 C CNN
F 2 "" H 6500 2150 50  0001 C CNN
F 3 "" H 6500 2150 50  0001 C CNN
	1    6500 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 61A5D424
P 8000 2150
F 0 "#PWR015" H 8000 1900 50  0001 C CNN
F 1 "GND" H 8005 1977 50  0000 C CNN
F 2 "" H 8000 2150 50  0001 C CNN
F 3 "" H 8000 2150 50  0001 C CNN
	1    8000 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 61A5E14D
P 8300 1650
F 0 "#PWR017" H 8300 1400 50  0001 C CNN
F 1 "GND" H 8305 1477 50  0000 C CNN
F 2 "" H 8300 1650 50  0001 C CNN
F 3 "" H 8300 1650 50  0001 C CNN
	1    8300 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 1650 8300 1650
Connection ~ 8300 1650
Wire Wire Line
	7700 1650 8150 1650
Connection ~ 8150 1650
Wire Wire Line
	8150 1650 8300 1650
$Comp
L power:+3.3V #PWR016
U 1 1 61A6531F
P 8300 1200
F 0 "#PWR016" H 8300 1050 50  0001 C CNN
F 1 "+3.3V" H 8315 1373 50  0000 C CNN
F 2 "" H 8300 1200 50  0001 C CNN
F 3 "" H 8300 1200 50  0001 C CNN
	1    8300 1200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR011
U 1 1 61A65F37
P 6500 1300
F 0 "#PWR011" H 6500 1150 50  0001 C CNN
F 1 "+3.3V" H 6515 1473 50  0000 C CNN
F 2 "" H 6500 1300 50  0001 C CNN
F 3 "" H 6500 1300 50  0001 C CNN
	1    6500 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 1300 6500 1350
Wire Wire Line
	8300 1200 8300 1250
Connection ~ 8300 1250
Wire Wire Line
	8300 1250 8550 1250
Wire Wire Line
	7700 1250 8150 1250
Text Label 7850 3800 0    50   ~ 0
usbD-
Text Label 7850 3700 0    50   ~ 0
usbD+
Text Label 7850 3500 0    50   ~ 0
usbHostEn
Wire Wire Line
	7750 3500 7850 3500
Wire Wire Line
	7750 3700 7850 3700
Wire Wire Line
	7750 3800 7850 3800
Text HLabel 7850 2700 2    50   Input ~ 0
spiSck
Text HLabel 7850 2800 2    50   Input ~ 0
spiMosi
Text HLabel 8450 3900 2    50   Input ~ 0
scl
Text HLabel 8800 4000 2    50   Input ~ 0
sda
Text HLabel 6650 3600 0    50   Input ~ 0
spiMiso
Text HLabel 6650 3400 0    50   Input ~ 0
tx
Text HLabel 6650 3500 0    50   Input ~ 0
rx
$Comp
L Device:R_US R1
U 1 1 61A7B26A
P 6400 4650
F 0 "R1" V 6500 4600 50  0000 L CNN
F 1 "10K" V 6300 4550 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6440 4640 50  0001 C CNN
F 3 "~" H 6400 4650 50  0001 C CNN
	1    6400 4650
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR09
U 1 1 61A7F3F1
P 6100 4650
F 0 "#PWR09" H 6100 4500 50  0001 C CNN
F 1 "+3.3V" H 6115 4823 50  0000 C CNN
F 2 "" H 6100 4650 50  0001 C CNN
F 3 "" H 6100 4650 50  0001 C CNN
	1    6100 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 4650 6250 4650
Wire Wire Line
	6600 4650 6750 4650
Text Label 6600 4650 0    50   ~ 0
rst
Wire Wire Line
	6650 3400 6750 3400
Wire Wire Line
	6650 3500 6750 3500
Wire Wire Line
	6650 3600 6750 3600
Wire Wire Line
	7850 3400 7750 3400
Wire Wire Line
	7850 3300 7750 3300
Wire Wire Line
	7850 2800 7750 2800
Wire Wire Line
	7850 2700 7750 2700
$Comp
L SparkFun-Clocks:CRYSTAL-32.768KHZSMD-3.2X1.5 Y1
U 1 1 61A93EB5
P 5950 2500
F 0 "Y1" V 5950 2600 45  0000 C CNN
F 1 "CRYSTAL-32.768KHZSMD-3.2X1.5" V 6200 2600 45  0000 C CNN
F 2 "CRYSTAL-SMD-3.2X1.5MM" H 5950 2700 20  0001 C CNN
F 3 "" H 5950 2500 50  0001 C CNN
F 4 "XTAL-13062" V 6300 2500 60  0000 C CNN "Field4"
	1    5950 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6750 2400 5950 2400
Wire Wire Line
	6750 2500 6100 2500
Wire Wire Line
	6100 2500 6100 2600
Wire Wire Line
	6100 2600 5950 2600
Wire Wire Line
	5950 2650 5950 2600
Connection ~ 5950 2600
Wire Wire Line
	5950 2400 5500 2400
Connection ~ 5950 2400
$Comp
L power:GND #PWR08
U 1 1 61AA5D39
P 5950 2900
F 0 "#PWR08" H 5950 2650 50  0001 C CNN
F 1 "GND" H 5955 2727 50  0000 C CNN
F 2 "" H 5950 2900 50  0001 C CNN
F 3 "" H 5950 2900 50  0001 C CNN
	1    5950 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 61AA9AC3
P 5500 2700
F 0 "#PWR07" H 5500 2450 50  0001 C CNN
F 1 "GND" H 5505 2527 50  0000 C CNN
F 2 "" H 5500 2700 50  0001 C CNN
F 3 "" H 5500 2700 50  0001 C CNN
	1    5500 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2700 5500 2650
Wire Wire Line
	5950 2900 5950 2850
$Comp
L TL1015AF160QG:TL1015AF160QG S1
U 1 1 61AB31BB
P 5650 5100
F 0 "S1" H 5650 5467 50  0000 C CNN
F 1 "TL1015AF160QG" H 5650 5376 50  0000 C CNN
F 2 "others:TL1015AF160QG" H 5650 5100 50  0001 L BNN
F 3 "" H 5650 5100 50  0001 L BNN
F 4 "F" H 5650 5100 50  0001 L BNN "PARTREV"
F 5 "E-Switch" H 5650 5100 50  0001 L BNN "MANUFACTURER"
F 6 "2.1 mm" H 5650 5100 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 7 "Manufacturer Recommendations" H 5650 5100 50  0001 L BNN "STANDARD"
	1    5650 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 5100 6100 5100
Text Label 6100 5100 0    50   ~ 0
rst
$Comp
L power:GND #PWR06
U 1 1 61AB870E
P 5300 5150
F 0 "#PWR06" H 5300 4900 50  0001 C CNN
F 1 "GND" H 5305 4977 50  0000 C CNN
F 2 "" H 5300 5150 50  0001 C CNN
F 3 "" H 5300 5150 50  0001 C CNN
	1    5300 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 5100 5300 5100
Wire Wire Line
	5300 5100 5300 5150
$Comp
L Device:R_US R2
U 1 1 61AD0B9B
P 8350 3750
F 0 "R2" H 8418 3796 50  0000 L CNN
F 1 "10K" H 8418 3705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8390 3740 50  0001 C CNN
F 3 "~" H 8350 3750 50  0001 C CNN
	1    8350 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R3
U 1 1 61AD303A
P 8700 3750
F 0 "R3" H 8768 3796 50  0000 L CNN
F 1 "10K" H 8768 3705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8740 3740 50  0001 C CNN
F 3 "~" H 8700 3750 50  0001 C CNN
	1    8700 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3900 8350 3900
Wire Wire Line
	7750 4000 8700 4000
Wire Wire Line
	8450 3900 8350 3900
Connection ~ 8350 3900
Wire Wire Line
	8700 3900 8700 4000
Connection ~ 8700 4000
Wire Wire Line
	8700 4000 8800 4000
$Comp
L power:+3.3V #PWR018
U 1 1 61AEA303
P 8550 3500
F 0 "#PWR018" H 8550 3350 50  0001 C CNN
F 1 "+3.3V" H 8565 3673 50  0000 C CNN
F 2 "" H 8550 3500 50  0001 C CNN
F 3 "" H 8550 3500 50  0001 C CNN
	1    8550 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 3600 8350 3550
Wire Wire Line
	8350 3550 8550 3550
Wire Wire Line
	8550 3550 8550 3500
Wire Wire Line
	8550 3550 8700 3550
Wire Wire Line
	8700 3550 8700 3600
Connection ~ 8550 3550
$Comp
L Diode:MBR0520 D1
U 1 1 61B82239
P 2850 2000
F 0 "D1" H 2850 1783 50  0000 C CNN
F 1 "MBR0520" H 2850 1874 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 2850 1825 50  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/MBR0520~MBR0580(SOD123).pdf" H 2850 2000 50  0001 C CNN
	1    2850 2000
	-1   0    0    1   
$EndComp
Wire Wire Line
	2700 2000 2250 2000
Wire Wire Line
	6550 4650 6600 4650
Connection ~ 6600 4650
$Comp
L power:GND #PWR013
U 1 1 61B90B74
P 6600 5050
F 0 "#PWR013" H 6600 4800 50  0001 C CNN
F 1 "GND" H 6605 4877 50  0000 C CNN
F 2 "" H 6600 5050 50  0001 C CNN
F 3 "" H 6600 5050 50  0001 C CNN
	1    6600 5050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 61B98338
P 6250 3000
F 0 "#PWR010" H 6250 2750 50  0001 C CNN
F 1 "GND" H 6255 2827 50  0000 C CNN
F 2 "" H 6250 3000 50  0001 C CNN
F 3 "" H 6250 3000 50  0001 C CNN
	1    6250 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 3000 6250 2950
Wire Wire Line
	6250 2750 6250 2700
$Comp
L Device:C_Small C?
U 1 1 61BA2DD2
P 6500 1950
AR Path="/61A218D6/61BA2DD2" Ref="C?"  Part="1" 
AR Path="/61A2171E/61BA2DD2" Ref="C4"  Part="1" 
F 0 "C4" H 6400 1900 50  0000 L TNN
F 1 "0.1uF" H 6500 2000 50  0000 R BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6500 1950 50  0001 C CNN
F 3 "~" H 6500 1950 50  0001 C CNN
	1    6500 1950
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 61BA88DA
P 6250 2850
AR Path="/61A218D6/61BA88DA" Ref="C?"  Part="1" 
AR Path="/61A2171E/61BA88DA" Ref="C3"  Part="1" 
F 0 "C3" H 6150 2800 50  0000 L TNN
F 1 "0.1uF" H 6250 2900 50  0000 R BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6250 2850 50  0001 C CNN
F 3 "~" H 6250 2850 50  0001 C CNN
	1    6250 2850
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 61BAA7CB
P 6600 4850
AR Path="/61A218D6/61BAA7CB" Ref="C?"  Part="1" 
AR Path="/61A2171E/61BAA7CB" Ref="C5"  Part="1" 
F 0 "C5" H 6500 4800 50  0000 L TNN
F 1 "0.1uF" H 6600 4900 50  0000 R BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6600 4850 50  0001 C CNN
F 3 "~" H 6600 4850 50  0001 C CNN
	1    6600 4850
	-1   0    0    1   
$EndComp
Wire Wire Line
	6600 4950 6600 5050
Wire Wire Line
	6600 4650 6600 4750
Wire Wire Line
	8000 1800 8000 1850
Wire Wire Line
	8000 2050 8000 2150
$Comp
L Device:C_Small C?
U 1 1 61BA0F7F
P 8000 1950
AR Path="/61A218D6/61BA0F7F" Ref="C?"  Part="1" 
AR Path="/61A2171E/61BA0F7F" Ref="C7"  Part="1" 
F 0 "C7" H 7900 1900 50  0000 L TNN
F 1 "1uF" H 8000 2000 50  0000 R BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8000 1950 50  0001 C CNN
F 3 "~" H 8000 1950 50  0001 C CNN
	1    8000 1950
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 61BBF9DB
P 7700 1450
AR Path="/61A218D6/61BBF9DB" Ref="C?"  Part="1" 
AR Path="/61A2171E/61BBF9DB" Ref="C6"  Part="1" 
F 0 "C6" H 7600 1400 50  0000 L TNN
F 1 "0.1uF" H 7700 1500 50  0000 R BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7700 1450 50  0001 C CNN
F 3 "~" H 7700 1450 50  0001 C CNN
	1    7700 1450
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 61BC1968
P 8150 1450
AR Path="/61A218D6/61BC1968" Ref="C?"  Part="1" 
AR Path="/61A2171E/61BC1968" Ref="C8"  Part="1" 
F 0 "C8" H 8050 1400 50  0000 L TNN
F 1 "0.1uF" H 8150 1500 50  0000 R BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8150 1450 50  0001 C CNN
F 3 "~" H 8150 1450 50  0001 C CNN
	1    8150 1450
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 61BC38E8
P 8550 1450
AR Path="/61A218D6/61BC38E8" Ref="C?"  Part="1" 
AR Path="/61A2171E/61BC38E8" Ref="C9"  Part="1" 
F 0 "C9" H 8450 1400 50  0000 L TNN
F 1 "0.1uF" H 8550 1500 50  0000 R BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8550 1450 50  0001 C CNN
F 3 "~" H 8550 1450 50  0001 C CNN
	1    8550 1450
	-1   0    0    1   
$EndComp
Wire Wire Line
	8550 1250 8550 1350
Wire Wire Line
	8550 1550 8550 1650
Wire Wire Line
	8150 1550 8150 1650
Wire Wire Line
	8150 1250 8150 1350
Wire Wire Line
	7700 1250 7700 1350
Wire Wire Line
	7700 1550 7700 1650
Wire Wire Line
	6500 1800 6500 1850
Connection ~ 6500 1800
Wire Wire Line
	6500 2050 6500 2150
$Comp
L Device:C_Small C?
U 1 1 61BF0662
P 5950 2750
AR Path="/61A218D6/61BF0662" Ref="C?"  Part="1" 
AR Path="/61A2171E/61BF0662" Ref="C2"  Part="1" 
F 0 "C2" H 5850 2700 50  0000 L TNN
F 1 "20pF" H 5950 2800 50  0000 R BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5950 2750 50  0001 C CNN
F 3 "~" H 5950 2750 50  0001 C CNN
	1    5950 2750
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 61BF25D2
P 5500 2550
AR Path="/61A218D6/61BF25D2" Ref="C?"  Part="1" 
AR Path="/61A2171E/61BF25D2" Ref="C1"  Part="1" 
F 0 "C1" H 5400 2500 50  0000 L TNN
F 1 "20pF" H 5500 2600 50  0000 R BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5500 2550 50  0001 C CNN
F 3 "~" H 5500 2550 50  0001 C CNN
	1    5500 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	5500 2450 5500 2400
Text HLabel 7850 3000 2    50   Input ~ 0
A1
Text HLabel 7850 2900 2    50   Input ~ 0
A2
Text HLabel 6650 2600 0    50   Input ~ 0
A0
Text HLabel 6650 2800 0    50   Input ~ 0
A3
Text HLabel 6650 2900 0    50   Input ~ 0
A4
Text HLabel 7850 3200 2    50   Input ~ 0
A5
Wire Wire Line
	6250 2700 6450 2700
Text HLabel 6400 2650 0    50   Input ~ 0
Aref
Wire Wire Line
	6400 2650 6450 2650
Wire Wire Line
	6450 2650 6450 2700
Connection ~ 6450 2700
Wire Wire Line
	6450 2700 6750 2700
Text HLabel 6650 4100 0    50   Input ~ 0
D13
Text HLabel 6650 4300 0    50   Input ~ 0
D12
Text HLabel 6650 4000 0    50   Input ~ 0
D11
Text HLabel 6650 4200 0    50   Input ~ 0
D10
Text HLabel 6650 3100 0    50   Input ~ 0
D9
Text HLabel 6650 3000 0    50   Input ~ 0
D8
Text HLabel 6650 4500 0    50   Input ~ 0
D7
Text HLabel 6650 4400 0    50   Input ~ 0
D6
Text HLabel 6650 3900 0    50   Input ~ 0
D5
Text HLabel 6650 3800 0    50   Input ~ 0
D4
Text HLabel 6650 3300 0    50   Input ~ 0
D3
Text HLabel 6650 3200 0    50   Input ~ 0
D2
Wire Wire Line
	6650 2800 6750 2800
Wire Wire Line
	6650 2900 6750 2900
Wire Wire Line
	6650 3000 6750 3000
Wire Wire Line
	6650 3100 6750 3100
Wire Wire Line
	6650 3200 6750 3200
Wire Wire Line
	6650 3300 6750 3300
Wire Wire Line
	6650 3800 6750 3800
Wire Wire Line
	6650 3900 6750 3900
Wire Wire Line
	6650 4000 6750 4000
Wire Wire Line
	6650 4100 6750 4100
Wire Wire Line
	6650 4200 6750 4200
Wire Wire Line
	6650 4300 6750 4300
Wire Wire Line
	6650 4400 6750 4400
Wire Wire Line
	6650 4500 6750 4500
Wire Wire Line
	7750 3200 7850 3200
Wire Wire Line
	7750 3000 7850 3000
Wire Wire Line
	7750 2900 7850 2900
$Comp
L Connector:TestPoint TP?
U 1 1 61C14766
P 2500 4000
AR Path="/61A217B5/61C14766" Ref="TP?"  Part="1" 
AR Path="/61A2171E/61C14766" Ref="TP1"  Part="1" 
F 0 "TP1" V 2454 4188 50  0000 L CNN
F 1 "TestPoint" V 2545 4188 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2700 4000 50  0001 C CNN
F 3 "~" H 2700 4000 50  0001 C CNN
	1    2500 4000
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 61C1476C
P 2500 4200
AR Path="/61A217B5/61C1476C" Ref="TP?"  Part="1" 
AR Path="/61A2171E/61C1476C" Ref="TP2"  Part="1" 
F 0 "TP2" V 2454 4388 50  0000 L CNN
F 1 "TestPoint" V 2545 4388 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2700 4200 50  0001 C CNN
F 3 "~" H 2700 4200 50  0001 C CNN
	1    2500 4200
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 61C14772
P 2500 4400
AR Path="/61A217B5/61C14772" Ref="TP?"  Part="1" 
AR Path="/61A2171E/61C14772" Ref="TP3"  Part="1" 
F 0 "TP3" V 2454 4588 50  0000 L CNN
F 1 "TestPoint" V 2545 4588 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2700 4400 50  0001 C CNN
F 3 "~" H 2700 4400 50  0001 C CNN
	1    2500 4400
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 61C14778
P 2500 4600
AR Path="/61A217B5/61C14778" Ref="TP?"  Part="1" 
AR Path="/61A2171E/61C14778" Ref="TP4"  Part="1" 
F 0 "TP4" V 2454 4788 50  0000 L CNN
F 1 "TestPoint" V 2545 4788 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2700 4600 50  0001 C CNN
F 3 "~" H 2700 4600 50  0001 C CNN
	1    2500 4600
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 61C1477E
P 2500 4800
AR Path="/61A217B5/61C1477E" Ref="TP?"  Part="1" 
AR Path="/61A2171E/61C1477E" Ref="TP5"  Part="1" 
F 0 "TP5" V 2454 4988 50  0000 L CNN
F 1 "TestPoint" V 2545 4988 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2700 4800 50  0001 C CNN
F 3 "~" H 2700 4800 50  0001 C CNN
	1    2500 4800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61C14784
P 2100 4250
AR Path="/61A217B5/61C14784" Ref="#PWR?"  Part="1" 
AR Path="/61A2171E/61C14784" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 2100 4000 50  0001 C CNN
F 1 "GND" H 2105 4077 50  0000 C CNN
F 2 "" H 2100 4250 50  0001 C CNN
F 3 "" H 2100 4250 50  0001 C CNN
	1    2100 4250
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR02
U 1 1 61C18133
P 2100 3950
F 0 "#PWR02" H 2100 3800 50  0001 C CNN
F 1 "+3.3V" H 2115 4123 50  0000 C CNN
F 2 "" H 2100 3950 50  0001 C CNN
F 3 "" H 2100 3950 50  0001 C CNN
	1    2100 3950
	1    0    0    -1  
$EndComp
Text Label 2400 4800 2    50   ~ 0
rst
Text Label 7850 3300 0    50   ~ 0
swdio
Text Label 7850 3400 0    50   ~ 0
swclk
Wire Wire Line
	2400 4400 2500 4400
Wire Wire Line
	2400 4600 2500 4600
Text Label 2400 4600 2    50   ~ 0
swdio
Text Label 2400 4400 2    50   ~ 0
swclk
Wire Wire Line
	2400 4800 2500 4800
Wire Wire Line
	2100 4000 2100 3950
Wire Wire Line
	2100 4000 2500 4000
Wire Wire Line
	2500 4200 2100 4200
Wire Wire Line
	2100 4200 2100 4250
$Comp
L Connector:TestPoint TP?
U 1 1 61CCAE34
P 2500 5000
AR Path="/61A217B5/61CCAE34" Ref="TP?"  Part="1" 
AR Path="/61A2171E/61CCAE34" Ref="TP6"  Part="1" 
F 0 "TP6" V 2454 5188 50  0000 L CNN
F 1 "TestPoint" V 2545 5188 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2700 5000 50  0001 C CNN
F 3 "~" H 2700 5000 50  0001 C CNN
	1    2500 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	2100 5000 2100 4950
Wire Wire Line
	2100 5000 2500 5000
$Comp
L power:VCC #PWR?
U 1 1 61CCF9E9
P 2100 4950
AR Path="/61A2186F/61CCF9E9" Ref="#PWR?"  Part="1" 
AR Path="/61A2171E/61CCF9E9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2100 4800 50  0001 C CNN
F 1 "VCC" H 2115 5123 50  0000 C CNN
F 2 "" H 2100 4950 50  0001 C CNN
F 3 "" H 2100 4950 50  0001 C CNN
	1    2100 4950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
