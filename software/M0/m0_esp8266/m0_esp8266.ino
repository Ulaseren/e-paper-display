#include <ArduinoJson.h>
#include <RTCZero.h>

/* Create an rtc object */
RTCZero rtc;

/* Change these values to set the current initial time */
byte seconds = 0;
byte minutes = 0;
byte hours = 16;

/* Change these values to set the current initial date */
byte day = 15;
byte month = 6;
byte year = 15;

String agAdi = "VODAFONE_70CD";
String agSifresi = "ulaseren9519";
String gelen_veri;
String APIKEY = "d3b37759ba5b04cc332be81a56feedf1";
String CityID = "745042";  //Your City ID
String macAdd="";
void setup() {
  SerialUSB.begin(115200);
  while (!SerialUSB)
    ;
  delay(5000);
  SerialUSB.println("SerialUSB    :[OK]");
  SerialUSB.flush();

  // Start the software serial for communication with the ESP8266
  Serial1.begin(115200);
  Serial1.println("AT");
  Serial1.flush();
  SerialUSB.println("AT Yollandı");
  SerialUSB.flush();
  while (!Serial1.find("OK")) {
    Serial1.println("AT");
    SerialUSB.println("ESP8266 Bulunamadı.");
  }
  SerialUSB.println("OK Komutu Alındı");
  SerialUSB.println("Ayar Yapılıyor");
  SerialUSB.flush();
  // Serial1.println("AT+CWDHCP=1,0");
  // Serial1.flush();
  // while(!Serial1.find("OK")){
  //   Serial1.println("AT+CWDHCP=1,0");
  //   SerialUSB.println(".");
  // }
  Serial1.println("AT+CWMODE=1");
  Serial1.flush();
  while (!Serial1.find("OK")) {
    Serial1.println("AT+CWMODE=1");
    SerialUSB.println(".");
  }
  SerialUSB.println("Client olarak ayarlandı");

  SerialUSB.println("wifi Baglaniliyor...");
  SerialUSB.flush();
  Serial1.println("AT+CWJAP=\"" + agAdi + "\",\"" + agSifresi + "\"");
  Serial1.flush();
  while (!Serial1.find("OK"))
    ;
  SerialUSB.println("wifi Baglandi.");
  SerialUSB.flush();
  Serial1.println("AT+CIFSR");
  Serial1.flush();
  gelen_veri = Serial1.readStringUntil('O');
  char bufmac[gelen_veri.length()];
  gelen_veri.toCharArray(bufmac, sizeof(bufmac));
  char *pmac = bufmac;
  char *strMac;
  String dataMac[6];
  int iMac = 0;
  while ((strMac = strtok_r(pmac, "\"", &pmac)) != NULL) {
    dataMac[iMac] = String(strMac);
    //SerialUSB.println(dataMac[iMac]);
    iMac++;
  }
  macAdd=dataMac[3];
  
  SerialUSB.println("wifi kapandı...");
  SerialUSB.flush();
  Serial1.println("AT+CWQAP");
  Serial1.flush();
  while (!Serial1.find("OK"))
    ;
    
  SerialUSB.println("wifi mac ile Baglaniliyor...");
  SerialUSB.flush();
  Serial1.println("AT+CWJAP=\"" + agAdi + "\",\"" + agSifresi + "\",\"" + macAdd + "\"");
  Serial1.flush();
  while (!Serial1.find("OK"))
    ;
  SerialUSB.println("wifi Baglandi.");
  SerialUSB.println("Aga Baglaniliyor...");
  SerialUSB.flush();
  Serial1.println("AT+CIPMUX=0");
  Serial1.flush();
  while (!Serial1.find("OK")) {
    Serial1.println("AT+CIPMUX=0");
    SerialUSB.println(".");
  }

  SerialUSB.print("Connect with ");
  SerialUSB.flush();
  Serial1.println("AT+CIPSTART=\"TCP\",\"api.openweathermap.org\",80");
  Serial1.flush();
  while (!Serial1.find("OK")) {
    Serial1.println("AT+CIPSTART=\"TCP\",\"api.openweathermap.org\",80");
    Serial1.flush();
    //delay(10);
  }
  SerialUSB.println("OpenWeatherMap");
  SerialUSB.flush();
  Serial1.println("AT+CIPSEND=87");
  Serial1.flush();
  while (!Serial1.find("OK"))
    ;
  SerialUSB.println("Data isteniyor");
  Serial1.println("GET /data/2.5/weather?id=" + CityID + "&units=metric&APPID=" + APIKEY);
  Serial1.flush();
  Serial1.println(" ");
  Serial1.flush();
  gelen_veri = Serial1.readString();
  gelen_veri.remove(0, 57);
  gelen_veri.replace('[', ' ');
  gelen_veri.replace(']', ' ');
  char jsonArray[gelen_veri.length() + 1];
  gelen_veri.toCharArray(jsonArray, sizeof(jsonArray));
  jsonArray[gelen_veri.length() + 1] = '\0';
  StaticJsonDocument<1024> doc;
  DeserializationError error = deserializeJson(doc, jsonArray);
  Serial1.println("AT+CIPCLOSE");
  // String location = doc["name"];
  // String country = doc["sys"]["country"];
  // int temperature = doc["main"]["temp"];
  // int humidity = doc["main"]["humidity"];
  // float pressure = doc["main"]["pressure"];

  // int id = doc["id"];
  // float Speed = doc["wind"]["speed"];
  // int degree = doc["wind"]["deg"];
  // float longitude = doc["coord"]["lon"];
  // float latitude = doc["coord"]["lat"];

  // SerialUSB.println();
  // SerialUSB.print("Country: ");
  // SerialUSB.println(country);
  // SerialUSB.print("Location: ");
  // SerialUSB.println(location);
  // SerialUSB.print("Location ID: ");
  // SerialUSB.println(id);
  // SerialUSB.print("Temperature:");
  // SerialUSB.println(temperature);
  // SerialUSB.print("Humidity:");
  // SerialUSB.println(humidity);
  // SerialUSB.print("Pressure:");
  // SerialUSB.println(pressure);
  // SerialUSB.print("Wind speed:");
  // SerialUSB.println(Speed);
  // SerialUSB.print("Wind degree:");
  // SerialUSB.println(degree);
  // SerialUSB.print("Longitude:");
  // SerialUSB.println(longitude);
  // SerialUSB.print("Latitude:");
  // SerialUSB.println(latitude);
  

  SerialUSB.flush();
  //delay(1000);
  Serial1.println("AT+CIPMUX=1");
  Serial1.flush();
  while (!Serial1.find("OK")) {
    Serial1.println("AT+CIPMUX=1");
    SerialUSB.println(".");
  }

  SerialUSB.print("Connect with ");
  SerialUSB.flush();
  Serial1.println("AT+CIPSTART=4,\"TCP\",\"www.google.com\",80");
  Serial1.flush();
  while (!Serial1.find("OK")) {
    Serial1.println("AT+CIPSTART=4,\"TCP\",\"www.google.com\",80");
    Serial1.flush();
    //delay(10);
  }
  SerialUSB.println("google");
  SerialUSB.flush();
  Serial1.println("AT+CIPSEND=4,18");
  Serial1.flush();
  while (!Serial1.find("OK"))
    ;
  SerialUSB.println("Data isteniyor");
  Serial1.println("              ");
  Serial1.println(" ");
  Serial1.flush();
  gelen_veri = Serial1.readStringUntil('G');
  gelen_veri.remove(0, 145);
  SerialUSB.println(gelen_veri);
  SerialUSB.flush();
  //delay(10);
  char buf[gelen_veri.length()];
  gelen_veri.toCharArray(buf, sizeof(buf));
  char *p = buf;
  char *str;
  String timeData[6];
  int i = 0;
  while ((str = strtok_r(p, " ", &p)) != NULL) {
    timeData[i] = String(str);
    //SerialUSB.println(timeData[i]);
    i++;
  }
  timeData[4].remove(0, 2);
  year = timeData[4].toInt();
  month = get_month_index(timeData[3]);
  day = timeData[2].toInt();
  timeData[5] = timeData[5] + "\n";
  char buf2[timeData[5].length()];
  timeData[5].toCharArray(buf2, sizeof(buf2));
  char *k = buf2;
  i = 0;
  while ((str = strtok_r(k, ":", &k)) != NULL) {
    timeData[i] = String(str);
    //SerialUSB.println(timeData[i]);
    i++;
  }
  seconds = timeData[2].toInt();
  minutes = timeData[1].toInt();
  hours = timeData[0].toInt();
  rtc.begin();
  rtc.setTime(hours, minutes, seconds);
  rtc.setDate(day, month, year);
  //delay(1000);
  print2digits(rtc.getDay());
  SerialUSB.print("/");
  print2digits(rtc.getMonth());
  SerialUSB.print("/");
  print2digits(rtc.getYear());
  SerialUSB.print(" ");

  // ...and time
  print2digits(rtc.getHours());
  SerialUSB.print(":");
  print2digits(rtc.getMinutes());
  SerialUSB.print(":");
  print2digits(rtc.getSeconds());
}

void print2digits(int number) {
  if (number < 10) {
    SerialUSB.print("0");  // print a 0 before if the number is < than 10
  }
  SerialUSB.print(number);
}

int get_month_index(String name) {

  if (name == "Jan") 
    return 1;
  else if (name == "Feb")
    return 2;
  else if (name == "Mar")
    return 3;
  else if (name == "Apr")
    return 4;
  else if (name == "May")
    return 5;
  else if (name == "Jun")
    return 6;
  else if (name == "Jul")
    return 7;
  else if (name == "Aug")
    return 8;
  else if (name == "Sep")
    return 9;
  else if (name == "Oct")
    return 10;
  else if (name == "Nov")
    return 11;
  else if (name == "Dec")
    return 12;
  else
    return -1;
}

void loop() {
}
