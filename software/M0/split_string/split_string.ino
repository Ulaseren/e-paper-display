String serialResponse = "";
char sz[] = "Here; is some; sample;100;data;1.414;1020";

void setup()
{
 SerialUSB.begin(115200);
 delay(5000);
 SerialUSB.setTimeout(5);
}

void loop()
{
  if ( SerialUSB.available()) {
    serialResponse = SerialUSB.readStringUntil('\r\n');

    // Convert from String Object to String.
    char buf[sizeof(sz)];
    serialResponse.toCharArray(buf, sizeof(buf));
    char *p = buf;
    char *str;
    while ((str = strtok_r(p, " ", &p)) != NULL) // delimiter is the semicolon
      SerialUSB.println(str);
  }
}