

void setup() {
  // Open serial communications and wait for port to open:
  SerialUSB.begin(115200);
  while (!SerialUSB) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  SerialUSB.println("Started");

  // set the data rate for the SoftwareSerial port
  Serial1.begin(115200);
  Serial1.write("AT\r\n");
}

void loop() {
  if (Serial1.available()) {
    SerialUSB.write(Serial1.read());
  }
  if (SerialUSB.available()) {
    Serial1.write(SerialUSB.read());
  }
}
