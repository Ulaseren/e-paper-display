
#define CH_PD_PIN 13  // Arduino pin connected to CH_PD (sometimes marked EN) pin on ESP8266
#define GPIO0_PIN 11  // Arduino pin connected to GPIO0 pin on ESP8266
#define RESET_PIN 12 // Arduino pin connected to RESET pin on ESP8266
void setup() 
{
  SerialUSB.begin(115200);
  Serial1.begin(115200);  
  
  pinMode(CH_PD_PIN, OUTPUT);
  pinMode(GPIO0_PIN, OUTPUT);
  pinMode(RESET_PIN, OUTPUT);
  digitalWrite(CH_PD_PIN,0);
  digitalWrite(GPIO0_PIN,1);
  digitalWrite(RESET_PIN,1);
   
  
  delay(1000);
  // Set GPIO0 low, to signal that on next boot, the ESP8266 should enter flashing mode
  digitalWrite(GPIO0_PIN, 0);
  delay(100);

  // Reset ESP8266 again
  digitalWrite(RESET_PIN, 0);
  delay(100);
  digitalWrite(RESET_PIN, 1);
  delay(100);
  digitalWrite(GPIO0_PIN,1);
  delay(100);
  // Now the ESP8266 should be in flashing mode
  
    
    // while(!SerialUSB);
    // SerialUSB.println("SerialUSB    :[OK]");
    // SerialUSB.flush(); 
 
    // Start the software serial for communication with the ESP8266
    
 
    // SerialUSB.println("");
    // SerialUSB.println("Remember to to set Both NL & CR in the serial monitor.");
    // SerialUSB.println("Ready");
    // SerialUSB.println("");    
}

void loop() 
{
    // listen for communication from the ESP8266 and then write it to the serial monitor
    if ( Serial1.available() )   {  
      SerialUSB.write(Serial1.read());  
      SerialUSB.flush();
    }
 
    // listen for user input and send it to the ESP8266
    if ( SerialUSB.available() ){   
      Serial1.println(SerialUSB.readString()); 
      Serial1.flush();
    }
}
