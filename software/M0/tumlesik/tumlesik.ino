#include <SPI.h>
#include "epd2in9_V2.h"
#include "epdpaint.h"
#include "imagedata.h"
#include <ArduinoJson.h>
#include <RTCZero.h>

/* Create an rtc object */
RTCZero rtc;

/* Change these values to set the current initial time */
byte seconds = 1;
byte minutes = 2;
byte hours = 3;

/* Change these values to set the current initial date */
byte day = 11;
byte month = 5;
byte year = 21;

#define COLORED     0
#define UNCOLORED   1

/**
  * Due to RAM not enough in Arduino UNO, a frame buffer is not allowed.
  * In this case, a smaller image buffer is allocated and you have to 
  * update a partial display several times.
  * 1 byte = 8 pixels, therefore you have to set 8*N pixels at a time.
  */
unsigned char image[1024];
Paint timeS(image, 0, 0);    // width should be the multiple of 8 
unsigned char image2[1024];
Paint dateS(image2, 0, 0);    // width should be the multiple of 8 
Epd epd;
unsigned long time_start_ms;
unsigned long time_now_s;

void setup() {
  // put your setup code here, to run once:
  SerialUSB.begin(115200);
  if (epd.Init() != 0) {
      SerialUSB.print("e-Paper init failed");
      return;
  }
  
  epd.ClearFrameMemory(0xFF);   // bit set = white, bit reset = black
  epd.DisplayFrame();
  epd.SetFrameMemory_Base(appcent_big);
  epd.DisplayFrame();
  delay(1000);
  epd.ClearFrameMemory(0xFF);   // bit set = white, bit reset = black
  epd.DisplayFrame();

  /** 
   *  there are 2 memory areas embedded in the e-paper display
   *  and once the display is refreshed, the memory area will be auto-toggled,
   *  i.e. the next action of SetFrameMemory will set the other memory area
   *  therefore you have to set the frame memory and refresh the display twice.
   */
  
  
  rtc.begin();
  rtc.setTime(hours, minutes, seconds);
  rtc.setDate(day, month, year);
}

void loop() {
  // put your main code here, to run repeatedly:
  String day;
  day=print2digits(rtc.getDay())+"/"+print2digits(rtc.getMonth())+"/"+print2digits(rtc.getYear())+"\n";
  char bufD[day.length()];
  day.toCharArray(bufD, sizeof(bufD));
  String time;
  time=print2digits(rtc.getHours())+":"+print2digits(rtc.getMinutes())+"\n";
  char bufT[time.length()];
  time.toCharArray(bufT, sizeof(bufT));
  //---------------------------
  dateS.SetWidth(32);
  dateS.SetHeight(138);
  dateS.SetRotate(ROTATE_270);
  dateS.Clear(UNCOLORED);
  timeS.SetWidth(32);
  timeS.SetHeight(96);
  timeS.SetRotate(ROTATE_270);
  timeS.Clear(UNCOLORED);

  dateS.DrawStringAt(0, 4, bufD, &Font24, COLORED);
  epd.SetFrameMemory_Partial(dateS.GetImage(), 30, 72, dateS.GetWidth(), dateS.GetHeight());
  timeS.DrawStringAt(0, 4, bufT, &Font24, COLORED);
  epd.SetFrameMemory_Partial(timeS.GetImage(), 80, 86, timeS.GetWidth(), timeS.GetHeight());
  epd.DisplayFrame_Partial();
  

  delay(1000);
}

String print2digits(int number) {
  if (number < 10) {
    return "0"+String(number);
  }
  return String(number);
}

int get_month_index(String name) {

  if (name == "Jan") 
    return 1;
  else if (name == "Feb")
    return 2;
  else if (name == "Mar")
    return 3;
  else if (name == "Apr")
    return 4;
  else if (name == "May")
    return 5;
  else if (name == "Jun")
    return 6;
  else if (name == "Jul")
    return 7;
  else if (name == "Aug")
    return 8;
  else if (name == "Sep")
    return 9;
  else if (name == "Oct")
    return 10;
  else if (name == "Nov")
    return 11;
  else if (name == "Dec")
    return 12;
  else
    return -1;
}
