/* Includes ------------------------------------------------------------------*/
#include "DEV_Config.h"
#include "EPD.h"
#include "GUI_Paint.h"
#include "imagedata.h"
#include <stdlib.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <WiFi.h>
#include "time.h"
#include <ArduinoJson.h>

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME280 bme;  // I2C

const char* ssid = "VODAFONE_70CD";
const char* password = "ulaseren9519";

const char* ntpServer = "pool.ntp.org";
const long gmtOffset_sec = 3600;
const int daylightOffset_sec = 3600;

String APIKEY = "d3b37759ba5b04cc332be81a56feedf1";
String CityID = "745042";          //Your City ID
bool id = false;
WiFiClient client;
char servername[] = "api.openweathermap.org";            // remote server we will connect to
String result;
int hour[3];
int date[3];
PAINT_TIME sPaint_time;
struct tm timeinfo;

UBYTE* BlackImage;
/* Entry point ----------------------------------------------------------------*/
void setup() {
  //Serial.begin(115200);
  //Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  if (!getLocalTime(&timeinfo)) {
    return;
  }
  //disconnect WiFi as it's no longer needed
  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);

  bme.begin(0x76);
  printf("EPD_2IN9_V2_test Demo\r\n");
  DEV_Module_Init();

  printf("e-Paper Init and Clear...\r\n");
  EPD_2IN9_V2_Init();
  EPD_2IN9_V2_Clear();
  DEV_Delay_ms(500);

  //Create a new image cache

  /* you have to edit the startup_stm32fxxx.s file and set a big enough heap size */
  UWORD Imagesize = ((EPD_2IN9_V2_WIDTH % 8 == 0) ? (EPD_2IN9_V2_WIDTH / 8) : (EPD_2IN9_V2_WIDTH / 8 + 1)) * EPD_2IN9_V2_HEIGHT;
  if ((BlackImage = (UBYTE*)malloc(Imagesize)) == NULL) {
    printf("Failed to apply for black memory...\r\n");
    while (1)
      ;
  }
  printf("Paint_NewImage\r\n");

  Paint_NewImage(BlackImage, EPD_2IN9_V2_WIDTH, EPD_2IN9_V2_HEIGHT, 270, WHITE);
  printf("show image for array\r\n");
  Paint_SelectImage(BlackImage);
  Paint_Clear(WHITE);
  Paint_DrawImage(appcent_big, 0, 0, 124, 297);

  EPD_2IN9_V2_Display(BlackImage);
  DEV_Delay_ms(2000);

  //EPD_2IN9_V2_Clear();
  printf("Drawing\r\n");
  Paint_SelectImage(BlackImage);
  Paint_Clear(WHITE);
  EPD_2IN9_V2_Display_Base(BlackImage);
}

/* The main loop -------------------------------------------------------------*/
void loop() {
  time();
  readBme();
  onlineWeather();
}
void time(){
  for (int i=0;i<3;i++) {
    if (!getLocalTime(&timeinfo)) {
      return;
    }
    hour[0] = timeinfo.tm_hour + 2;
    hour[1] = timeinfo.tm_min;
    hour[2] = timeinfo.tm_sec;
    sPaint_time.Hour = hour[0];
    sPaint_time.Min = hour[1];
    sPaint_time.Sec = hour[2];

    Paint_ClearWindows(10, 80, 10 + Font20.Width * 7, 80 + Font20.Height, WHITE);
    Paint_DrawTime(10, 80, &sPaint_time, &Font20, WHITE, BLACK);
    EPD_2IN9_V2_Display_Partial(BlackImage);
  }
  Paint_ClearWindows(10, 80, 10 + Font20.Width * 7, 80 + Font20.Height, WHITE);
  EPD_2IN9_V2_Display_Partial(BlackImage);
}
void readBme(){
  for (int i=0;i<3;i++) {
    String temp = String(bme.readTemperature());
    char tempB[50];
    temp.toCharArray(tempB, 50);
    String press = String(bme.readPressure());
    char presB[50];
    press.toCharArray(presB, 50);
    String hum = String(bme.readHumidity());
    char humB[50];
    hum.toCharArray(humB, 50);
    Paint_ClearWindows(10, 0, 10 + Font20.Width * 7, 0 + Font20.Height, WHITE);
    Paint_DrawString_EN(10, 0, tempB, &Font20, WHITE, BLACK);
    Paint_ClearWindows(10, 20, 10 + Font20.Width * 9, 20 + Font20.Height, WHITE);
    Paint_DrawString_EN(10, 20, presB, &Font20, WHITE, BLACK);
    Paint_ClearWindows(10, 40, 10 + Font20.Width * 7, 40 + Font20.Height, WHITE);
    Paint_DrawString_EN(10, 40, humB, &Font20, WHITE, BLACK);
    EPD_2IN9_V2_Display_Partial(BlackImage);
  }
  Paint_ClearWindows(10, 0, 10 + Font20.Width * 7, 0 + Font20.Height, WHITE);
  Paint_ClearWindows(10, 20, 10 + Font20.Width * 9, 20 + Font20.Height, WHITE);
  Paint_ClearWindows(10, 40, 10 + Font20.Width * 7, 40 + Font20.Height, WHITE); 
  EPD_2IN9_V2_Display_Partial(BlackImage); 
  
}
void onlineWeather(){
  delay(500);
  printf("111111 \r\n");
  EPD_2IN9_V2_Sleep();
  delay(500);
  printf("222222 \r\n");
  delay(500);
  WiFi.mode(WIFI_STA);   //   create wifi station
  delay(500);
  printf("333333\r\n");
  // //Serial.println(ssid);
  WiFi.begin(ssid, password);
  printf("4444444\r\n");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  if (client.connect(servername, 80))
  { //starts client connection, checks for connection
    client.println("GET /data/2.5/weather?id=" + CityID + "&units=metric&APPID=" + APIKEY);
    client.println("Host: api.openweathermap.org");
    client.println("User-Agent: ArduinoWiFi/1.1");
    client.println("Connection: close");
    client.println();
  }
  else {
    printf("connection failed\r\n");        //error message if no client connect
  }

  while (client.connected() && !client.available())
    delay(1);                                          //waits for data
  while (client.connected() || client.available())
  { //connected or data available
    char c = client.read();                     //gets byte from ethernet buffer
    result = result + c;
  }

  client.stop();                                      //stop client
  result.replace('[', ' ');
  result.replace(']', ' ');
  //Serial.println(result);
  char jsonArray [result.length() + 1];
  result.toCharArray(jsonArray, sizeof(jsonArray));
  jsonArray[result.length() + 1] = '\0';
  StaticJsonDocument<1024> doc;
  DeserializationError  error = deserializeJson(doc, jsonArray);


  if (error) {
    printf("deserializeJson() failed with code \r\n");
    //printf(error.c_str());
    return;
  }

  String location = doc["name"];
  String country = doc["sys"]["country"];
  int temperature = doc["main"]["temp"];
  int humidity = doc["main"]["humidity"];
  float pressure = doc["main"]["pressure"];
  int id = doc["id"];
  float Speed = doc["wind"]["speed"];
  int degree = doc["wind"]["deg"];
  float longitude = doc["coord"]["lon"];
  float latitude = doc["coord"]["lat"];

  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
  EPD_2IN9_V2_Init();
  char locationB[50];
  location.toCharArray(locationB, 50);  
  char countryB[50];
  country.toCharArray(countryB, 50);
  String temp = String(temperature);
  char temB[50];
  temp.toCharArray(temB, 50);
  String hum = String(humidity);
  char humB[50];
  hum.toCharArray(humB, 50);
  String press = String(pressure);
  char pressB[50];
  press.toCharArray(pressB, 50);
  String speed = String(Speed);
  char speedB[50];
  speed.toCharArray(speedB, 50);
  
  Paint_ClearWindows(10, 0, 10 + Font20.Width * 7, 0 + Font20.Height, WHITE);
  Paint_DrawString_EN(10, 0, locationB, &Font20, WHITE, BLACK);
  
  Paint_ClearWindows(30, 0, 30 + Font20.Width * 7, 0 + Font20.Height, WHITE);
  Paint_DrawString_EN(30, 0, countryB, &Font20, WHITE, BLACK);

  Paint_ClearWindows(10, 20, 10 + Font20.Width * 9, 20 + Font20.Height, WHITE);
  Paint_DrawString_EN(10, 20, temB, &Font20, WHITE, BLACK);
  
  Paint_ClearWindows(10, 40, 10 + Font20.Width * 7, 40 + Font20.Height, WHITE);
  Paint_DrawString_EN(10, 40, humB, &Font20, WHITE, BLACK);

  Paint_ClearWindows(10, 60, 10 + Font20.Width * 7, 60 + Font20.Height, WHITE);
  Paint_DrawString_EN(10, 60, pressB, &Font20, WHITE, BLACK);

  Paint_ClearWindows(10, 80, 10 + Font20.Width * 9, 80 + Font20.Height, WHITE);
  Paint_DrawString_EN(10, 80, speedB, &Font20, WHITE, BLACK);

  EPD_2IN9_V2_Display_Partial(BlackImage);
  DEV_Delay_ms(2000); 
Paint_ClearWindows(10, 0, 10 + Font20.Width * 7, 0 + Font20.Height, WHITE);  
Paint_ClearWindows(30, 0, 30 + Font20.Width * 7, 0 + Font20.Height, WHITE);
Paint_ClearWindows(10, 20, 10 + Font20.Width * 9, 20 + Font20.Height, WHITE);
Paint_ClearWindows(10, 40, 10 + Font20.Width * 7, 40 + Font20.Height, WHITE);
Paint_ClearWindows(10, 60, 10 + Font20.Width * 7, 60 + Font20.Height, WHITE);
Paint_ClearWindows(10, 80, 10 + Font20.Width * 9, 80 + Font20.Height, WHITE);
EPD_2IN9_V2_Display_Partial(BlackImage);

}