/*=====================================================================================
 |       Author:  İbrahim VAROLA
 |     Language:  C/C++ - Arduino 1.8.15
 +-------------------------------------------------------------------------------------
 |
 |  Description:  ESP32 tabanlı geliştirme kartları için e-paper kullanılarak
 |                bağlı sensörler tarafından sıcaklık, nem, CO2 gibi veriler,
 |                NTPClient üzerinden alınan tarih ve saat bilgisi yazdırılması
 |                amaçlanmıştır.
 |        Input:  PIR, CO2 ve BME280 sensörleri
 |
 |       Output:  Waveshare 2.9' e-paper display (Black - White - Yellow)
 *=====================================================================================*/


/*=============================================================================
 |      BU NOKTADA PROGRAM İÇİN GEREKLİ KÜTÜPHANELER TANIMLANDI
 ==============================================================================*/
#include <WiFi.h>
#include <NTPClient.h>       // Library Manager üzerinden 3.1.0 sürümü kurulmalı
#include <WiFiUdp.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h> // Library Manager üzerinden "Adafruit Unified Sensor" kütüphanesi kurulmalı
#include <Adafruit_BME280.h> // Library Manager üzerinden "Adafruit BME280 Library" kütüphanesi kurulmalı
#include <GxEPD2_3C.h>       // Library Manager üzerinden "GxEPD2" kütüphanesi kurulmalı ve kullanılacak ekrana göre kütüphane tanımlanması yapılmalı
                             // Kullanılacak ekran için yapılması gereken seçim noktasında GxEPD2 Example skecine bakınız
#include "user_interface.h"  // Ekranda bulunan kullanıcı arayüzünün bulunmuş olduğu kütühane tanımlandı.
#include "arial7pt7b.h"      // https://rop.nl/truetype2gfx/ üzerinden 7 puntoluk Arial fontu oluşturuldu.
 
/*======================================================================================
 |      BU NOKTADA PROGRAM İÇİN GEREKLİ GİRİŞ - ÇIKIŞ TANIMLAMALARI YAPILDI
 =======================================================================================*/
#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10
#define PIR_SENSOR 13
#define CO2_SENSOR 27
#define PIR_LED 33
#define CO2_LED 25
#define SEALEVELPRESSURE_HPA (1013.25)

/*==================================================================================================
 |      BU NOKTADA ÇİFT ÇEKİRDEĞİN KULLANIMI İÇİN GEREKLİ OLAN GÖREV TANIMLAMALARI YAPILDI
 ===================================================================================================*/
TaskHandle_t EPD;
TaskHandle_t IO;

Adafruit_BME280 bme; // I2C hattından BME280 sensörünün bağlandığı belirtildi

/*==================================================================================================================
 |      BU NOKTADA E-PAPER EKRANIN TANIMLANMASI YAPILDI (GxEPD2 kütüphanesinde bulunan ekran modeli seçilmeli!)
 ===================================================================================================================*/
GxEPD2_3C<GxEPD2_290c, GxEPD2_290c::HEIGHT> display(GxEPD2_290c(/*CS=5*/ SS, /*DC=*/ 17, /*RST=*/ 16, /*BUSY=*/ 4)); // GDEW029Z10

// Kendi modeminizin SSID ve parolasını girin
const char* ssid     = "modemSSID";
const char* password = "modemSifre";

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

/*===============================================================
 |      BU NOKTADA VERİLERİ TUTAN DEĞİŞENLER TANIMLANDI
 ================================================================*/
String formattedDate;
String dayStamp;
String timeStamp;
String minuteData_str;
String hourData_str;
String hourDataCache_str;
String secondDataCache_str;
String secondData_str;
unsigned int counter=0;
bool state = 0;
unsigned int minuteData = 0;
unsigned int hourData = 0;

void setup() 
{
  pinMode(PIR_SENSOR, INPUT);
  pinMode(PIR_LED, OUTPUT);
  pinMode(CO2_SENSOR, INPUT);
  pinMode(CO2_LED, OUTPUT);
  
  display.init(115200);
  Serial.begin(115200);

 /*==================================================
 |      BU NOKTADA Wi-Fi AĞI BAĞLANTISI YAPILDI
 ====================================================*/
  Serial.print(ssid);
  Serial.println(" agina baglaniliyor");
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.println("WiFi baglantisi kuruldu.");
  Serial.println("IP adresi: ");
  Serial.println(WiFi.localIP());

/*===============================================================
 |      BU NOKTADA TARİH-SAAT VERİ AKIŞI BAŞLATILDI
 ================================================================*/
  timeClient.begin(); //Zaman verisini toplama işlemi başlatıldı
  // Kendi zaman diliminize göre offset değerini girin
  // GMT +1 = 3600
  // GMT +8 = 28800
  // GMT -1 = -3600
  // GMT 0 = 0
  timeClient.setTimeOffset(10800);

/*===================================================================
 |      BU NOKTADA BME280 SENSÖRÜNÜN BAĞLANTI KONTROLÜ YAPILDI
 ====================================================================*/
  unsigned status;
  status = bme.begin(0x76);
  if (!status) {
    Serial.println("BME280 sensoru bulunamadi. Lutfen baglantinizi, girmis oldugunuz adresi veya cihaz ID verisini kontrol edin!");
    while(1) delay(10);
  }
  else if(status)
  {
    Serial.println("BME280 baglantisi basarili!");
  }


    drawLogo(icon); // E-paper display üzerinde
    delay(2000);


  xTaskCreatePinnedToCore(
    IOTask,     /* Görevin işleneceği fonksiyon */
    "IO",       /* Görevin adı */
    10000,      /* Stack büyüklüğü */
    NULL,       /* Görevin giriş parametresi */
    1,          /* Görevin önceliği */
    &IO,        /* Task handle */
    0           /* Görevin yürütüleceği çekirdek */
    );

  delay(500);

  xTaskCreatePinnedToCore(
    EPDTask,
    "EPD",
    10000,
    NULL,
    10,
    &EPD,
    1
    );

}

/*=========================================================================
 |      BU NOKTADA ESP32' ye BAĞLANAN GİRİŞLERİN İŞLENMESİ SAĞLANDI
 ==========================================================================*/
void IOTask(void *pvParameters)
{

 /*=======================================================================================================
 |      BU NOKTADA GÖREV BİR LOOP İÇİNDE ÇALIŞMALI ANCAK BU DÖNGÜ WHILE İLE KURULURSA HATA ALINIR!!
 |      FOR DÖNGÜSÜYLE LOOP OLUŞTURUN!!!!
 =========================================================================================================*/
  for(;;){
    CO2(); // CO2 sensörünün işlendiği fonksiyon
    digitalWrite(PIR_LED, digitalRead(PIR_SENSOR)); // PIR sensöründen gelen veriye göre çıkıştaki LED çalıştırıldı
    delay(100);
  }
}

/*=============================================================================
 |      BU NOKTADA ESP32' ye BAĞLANAN E-PAPER' in İŞLENMESİ SAĞLANDI
 ==============================================================================*/
void EPDTask(void *pvParameters) 
{
  for(;;){
    dateTask(); // Tarih ve saat verilerinin işlendiği fonksiyon
    drawSensorStamp(); // E-paper display üzerine verilerin yazdırıldığı fonksiyon
  }
  
}

void loop() {
  
}

/*=========================================================================
 |      BU NOKTADA INTERNET ÜZERİNDEN ÇEKİLEN TARİH VE SAAT VERİLERİ 
 |      İŞLENMESİ SAĞLANAN FONKSİYON OLUŞTURULDU
 ==========================================================================*/
void dateTask() 
{
  while(!timeClient.update()) {
    timeClient.forceUpdate();
  }
    formattedDate = timeClient.getFormattedDate(); // İnternetten çekilen formatlı tarih ve saat verisi string değişkene atandı.

/*====================================================================================
 |      BU NOKTADAN FONKSİYONUN BİTİŞİNE KADAR STRING ÜZERİNDE KIRPMA VE KIRPILAN 
 |      BAZI VERİLERİN INTEGER BİR DEĞİŞKENE ATANMASI SAĞLANDI
 =====================================================================================*/
    int splitT = formattedDate.indexOf("T");
    dayStamp = formattedDate.substring(0, splitT);
    
    timeStamp = formattedDate.substring(splitT+1, formattedDate.length()-1);
    hourDataCache_str = timeStamp;
    timeStamp.remove(5,3);
    
    hourDataCache_str.remove(2,6);
    hourData_str = hourDataCache_str;
    hourData = hourData_str.toInt();
    
    minuteData_str = timeStamp;
    minuteData_str.remove(0,3);
    minuteData = minuteData_str.toInt();
}

void drawLogo(const unsigned char *bitmap) 
{
  display.setRotation(0);
  display.setFullWindow();
  display.firstPage();
  do{
    display.fillScreen(GxEPD_WHITE);
    display.drawInvertedBitmap(0, 0, bitmap, display.epd2.WIDTH, display.epd2.HEIGHT, GxEPD_BLACK);
    }
  while (display.nextPage());
}

/*=============================================================================
 |      BU NOKTADA ESP32' ye BAĞLANAN E-PAPER ÜZERİNE ESP32 TARAFINDAN
 |      TOPLANAN VE İŞLENEN VERİLERİN YAZDIRILDIĞI FONKSİYON OLUŞTURULDU
 ==============================================================================*/
void drawSensorStamp()
{  
  
  display.setRotation(1); // Ekran 90 derece döndürüldü
  display.setFont(&arial7pt7b); // Verilerin yazılacağı font seçildi
  display.setFullWindow(); // Tüm ekranın kullanılması sağlandı
  display.setTextColor(GxEPD_BLACK); // Ekranda yazılacak yazının rengi seçildi
  
/*================================================================================================
 |      EKRANA YAZDIRILACAK VERİ display.firstPage(); İLE DO-WHILE DONGUSU ARASINDA YAZDIRILIR
 =================================================================================================*/
  display.firstPage();
  do
  {
  display.fillScreen(GxEPD_BLACK); // Ekranın tamamı siyah hale getirildi
  delay(500);
  display.fillScreen(GxEPD_WHITE); // Ekranın tamamı beyaz hale getirildi
  display.drawInvertedBitmap(170, 0, appcent_small, 126, 31, GxEPD_BLACK);

  // Syntax = display.drawInvertedBitmap(x_konum, y_konum, ikon_degiskeni, ikon_x_boyutu, ikon_y_boyutu, ikon_rengi);
  // İkon oluşturulurken Image2Lcd yazılımından inverted seçeneği seçilmeli
  display.drawInvertedBitmap(7, 10, clock_logo, 25, 25, GxEPD_BLACK);
  display.setCursor(35, 26); // Yazdırılacak verilin x ve y koordinatındaki yeri belirlendi
  display.println(timeStamp); // Veri yazdırıldı

  display.drawInvertedBitmap(89, 13, co2, 25, 25, GxEPD_BLACK);
  display.setCursor(118, 26);
  display.println(counter);

  display.drawInvertedBitmap(7, 43, calendar, 25, 25, GxEPD_BLACK);
  display.setCursor(35, 59);
  display.println(dayStamp);

  display.drawInvertedBitmap(7, 78, humidity, 25, 34, GxEPD_BLACK);
  display.setCursor(37, 98);
  display.println(bme.readHumidity());
  
  display.drawInvertedBitmap(107, 70, temperature, 25, 50, GxEPD_BLACK);
  display.setCursor(138, 95);
  display.print(bme.readTemperature());

  if(counter < 50)
  {
    display.drawInvertedBitmap(200, 50, smile, 50, 50, GxEPD_BLACK);
  }
  else if (counter >= 50)
  {
    display.drawInvertedBitmap(200, 50, attention, 50, 50, GxEPD_BLACK);
  }
  }
  while (display.nextPage());
}

/*========================================================================================================
 |      CO2 VERİSİNİN OKUNDUĞU FOKSİYON OLUŞTURULDU
 |      BU NOKTADA SENSÖR BUTON TARAFINDAN SİMÜLE EDİLDİ VE BUTONUN DÜŞEN KENARI İLE ALGILAMA YAPILDI
 =========================================================================================================*/
void CO2() 
{
  if(digitalRead(CO2_SENSOR)){
      state = 1;
    }
    else if(digitalRead(CO2_SENSOR) == 0 && state == 1){
      counter += 10;
      state = 0;
    }
    else if(counter >= 50 && counter < 100){
      digitalWrite(CO2_LED, HIGH);
    }
    else if(counter >= 100) {
      digitalWrite(CO2_LED, LOW);
      counter = 0;
    }

}

/*============================================================
 |      DEBUG İÇİN BURADAKİ FONSİYON AKTİVE EDİLMELİ!
 =============================================================*/
/*
void printValues() {
    Serial.print("DATE: ");
    Serial.println(dayStamp);
  
    Serial.print("HOUR: ");
    Serial.println(timeStamp);

    Serial.print("HOUR - INT: ");
    Serial.println(hourData);
    
    Serial.print("MINUTE: ");
    Serial.println(minuteData);
  
    Serial.print("Temperature = ");
    Serial.print(bme.readTemperature());
    Serial.println(" C");

    Serial.print("Pressure = ");

    Serial.print(bme.readPressure() / 100.0F);
    Serial.println(" hPa");

    Serial.print("Approx. Altitude = ");
    Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
    Serial.println(" m");

    Serial.print("Humidity = ");
    Serial.print(bme.readHumidity());
    Serial.println(" percent");

    Serial.println();
    Serial.println(counter);
}
*/
