#include <WiFi.h>
#include "time.h"

const char* ssid       = "VODAFONE_70CD";
const char* password   = "ulaseren9519";

const char* ntpServer = "tr.pool.ntp.org";
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;

void printLocalTime()
{
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  int hour[3];
  int date[3];
  // hour[0]=timeinfo.tm_hour+2;
  // hour[1]=timeinfo.tm_min;
  
  // date[0]=timeinfo.tm_year+1900;
  // date[1]=timeinfo.tm_mon+1;
  // date[2]=timeinfo.tm_mday;
  // Serial.printf( "%d/%d/%d %d:%d\n",date[2],date[1],date[0],hour[0],hour[1]);
  //   int tm_sec;   // seconds after the minute - [0, 60] including leap second
  //   int tm_min;   // minutes after the hour - [0, 59]
  //   int tm_hour;  // hours since midnight - [0, 23]
  //   int tm_mday;  // day of the month - [1, 31]
  //   int tm_mon;   // months since January - [0, 11]
  //   int tm_year;  // years since 1900
  //   int tm_wday;  // days since Sunday - [0, 6]
  //   int tm_yday;  // days since January 1 - [0, 365]
  //   int tm_isdst; // daylight savings time flag
  // Serial.println(timeinfo.tm_mon+1);

  // Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
  String timea;
  hour[0]=timeinfo.tm_hour;
  hour[1]=timeinfo.tm_min;
  hour[2]=timeinfo.tm_sec;
  timea=String(hour[0])+":"+String(hour[1])+":"+String(hour[2]);
  Serial.println(timea);
  String datae;
  date[0]=timeinfo.tm_year+1900;
  date[1]=timeinfo.tm_mon+1;
  date[2]=timeinfo.tm_mday;
  datae=String(date[2])+"/"+String(date[1])+"/"+String(date[0]);
  Serial.println(datae);
}

void setup()
{
  Serial.begin(115200);
  
  //connect to WiFi
  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }
  Serial.println(" CONNECTED");
  
  //init and get the time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  printLocalTime();

  //disconnect WiFi as it's no longer needed
  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
}

void loop()
{
  delay(1000);
  printLocalTime();
}