/* Includes ------------------------------------------------------------------*/
#include "DEV_Config.h"
#include "EPD.h"
#include "GUI_Paint.h"
#include "imagedata.h"
#include <stdlib.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <WiFi.h>
#include "time.h"

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME280 bme;  // I2C

const char* ssid = "VODAFONE_70CD";
const char* password = "ulaseren9519";

const char* ntpServer = "pool.ntp.org";
const long gmtOffset_sec = 3600;
const int daylightOffset_sec = 3600;

int hour[3];
int date[3];
PAINT_TIME sPaint_time;
struct tm timeinfo;
UBYTE* BlackImage;
/* Entry point ----------------------------------------------------------------*/
void setup() {
  //Serial.begin(115200);
  //Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  if (!getLocalTime(&timeinfo)) {
    return;
  }
  //disconnect WiFi as it's no longer needed
  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
  bme.begin(0x76);
  printf("EPD_2IN9_V2_test Demo\r\n");
  DEV_Module_Init();

  printf("e-Paper Init and Clear...\r\n");
  EPD_2IN9_V2_Init();
  EPD_2IN9_V2_Clear();
  DEV_Delay_ms(500);

  //Create a new image cache

  /* you have to edit the startup_stm32fxxx.s file and set a big enough heap size */
  UWORD Imagesize = ((EPD_2IN9_V2_WIDTH % 8 == 0) ? (EPD_2IN9_V2_WIDTH / 8) : (EPD_2IN9_V2_WIDTH / 8 + 1)) * EPD_2IN9_V2_HEIGHT;
  if ((BlackImage = (UBYTE*)malloc(Imagesize)) == NULL) {
    printf("Failed to apply for black memory...\r\n");
    while (1)
      ;
  }
  printf("Paint_NewImage\r\n");

  Paint_NewImage(BlackImage, EPD_2IN9_V2_WIDTH, EPD_2IN9_V2_HEIGHT, 270, WHITE);
  printf("show image for array\r\n");
  Paint_SelectImage(BlackImage);
  Paint_Clear(WHITE);
  Paint_DrawImage(appcent_big, 0, 0, 124, 297);

  EPD_2IN9_V2_Display(BlackImage);
  DEV_Delay_ms(2000);

  //EPD_2IN9_V2_Clear();
  printf("Drawing\r\n");
  Paint_SelectImage(BlackImage);
  Paint_Clear(WHITE);
  //EPD_2IN9_V2_Clear();
  // printf("Drawing\r\n");
  // //1.Select Image
  // Paint_SelectImage(BlackImage);
  // Paint_Clear(WHITE);

  // 2.Drawing on the image
  //printf("Drawing:BlackImage\r\n");
  //Paint_DrawImage(appcent_small, 0, 0, 54, 126);
  //Paint_DrawImage(clock_logo,90, 20, 25, 25);
  //Paint_DrawImage(co2,90, 40, 25, 25);
  //Paint_DrawImage(humidity,90, 60, 25, 34);
  EPD_2IN9_V2_Display_Base(BlackImage);
  // DEV_Delay_ms(2000);
}

/* The main loop -------------------------------------------------------------*/
void loop() {
  for (int i=0;i<10;i++) {
    if (!getLocalTime(&timeinfo)) {
      return;
    }
    hour[0] = timeinfo.tm_hour + 2;
    hour[1] = timeinfo.tm_min;
    hour[2] = timeinfo.tm_sec;
    sPaint_time.Hour = hour[0];
    sPaint_time.Min = hour[1];
    sPaint_time.Sec = hour[2];

    Paint_ClearWindows(10, 80, 10 + Font20.Width * 7, 80 + Font20.Height, WHITE);
    Paint_DrawTime(10, 80, &sPaint_time, &Font20, WHITE, BLACK);
    EPD_2IN9_V2_Display_Partial(BlackImage);
  }
  Paint_ClearWindows(10, 80, 10 + Font20.Width * 7, 80 + Font20.Height, WHITE);
  for (int i=0;i<10;i++) {
    String string1 = String(bme.readTemperature());
    char Buf1[50];
    string1.toCharArray(Buf1, 50);
    String string2 = String(bme.readPressure());
    char Buf2[50];
    string2.toCharArray(Buf2, 50);
    String string3 = String(bme.readHumidity());
    char Buf3[50];
    string3.toCharArray(Buf3, 50);
    Paint_ClearWindows(10, 0, 10 + Font20.Width * 7, 0 + Font20.Height, WHITE);
    Paint_DrawString_EN(10, 0, Buf1, &Font20, WHITE, BLACK);
    Paint_ClearWindows(10, 20, 10 + Font20.Width * 9, 20 + Font20.Height, WHITE);
    Paint_DrawString_EN(10, 20, Buf2, &Font20, WHITE, BLACK);
    Paint_ClearWindows(10, 40, 10 + Font20.Width * 7, 40 + Font20.Height, WHITE);
    Paint_DrawString_EN(10, 40, Buf3, &Font20, WHITE, BLACK);
    EPD_2IN9_V2_Display_Partial(BlackImage);
  }
  Paint_ClearWindows(10, 0, 10 + Font20.Width * 7, 0 + Font20.Height, WHITE);
  Paint_ClearWindows(10, 20, 10 + Font20.Width * 9, 20 + Font20.Height, WHITE);
  Paint_ClearWindows(10, 40, 10 + Font20.Width * 7, 40 + Font20.Height, WHITE);
      
}
